<?php

namespace InfinityTree;

class Node
{
	public $id = null;
	public $parent = null;
	public $tree = null;
	public $slug = '';
	public $name = '';
	public $path = array();
	public $children = array();
	
	public function inject_data(Array $incoming)
	{
		$object_vars = get_object_vars($this);
		
		foreach ( $object_vars as $key => $value )
		{
			if ( array_key_exists($key, $incoming) )
			{
				$this->$key = $incoming[$key];
			}
		}
	}
	
	public function extract_data(Array $outgoing)
	{
		foreach ( $outgoing as $key => $value )
		{
			if ( property_exists($this, $key) )
			{
				$outgoing[$key] = $this->$key;
			}
		}
		
		return $outgoing;
	}
}