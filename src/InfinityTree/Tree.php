<?php

namespace InfinityTree;

use InfinityTree\Node;
use InfinityTree\Interfaces\Storage as StorageInterface;

class Tree
{
	private $_storage = null;
	
	private $_tree_id = null;
	
	private $_errors = array();
	
	private $_root = null;
	
	public function __construct($tree_id, StorageInterface $storage)
	{
		$this->_tree_id = (int)$tree_id;
		$this->_storage = $storage;
		
		$nodes = $this->_storage->getNode(array('tree' => $this->_tree_id, 'slug' => 'root'));
		
		if ( count($nodes) > 0 )
		{
			$this->_root = $nodes[0];
		}
		else
		{
			$node = new Node();
			$node->tree = $this->_tree_id;
			$node->path[] = 0;
			$node->parent = 0;
			$node->slug = 'root';
			$node->name = 'Root';
			$node->children = array();
			$this->_root = $this->_storage->addNode($node);
		}
	}
	
	public function getNode($id)
	{
		$nodes = $this->_storage->getNode(array('id' => (int)$id));
		
		if ( count($nodes) > 0 )
		{
			return $nodes[0];
		}
		
		$node = new Node();
		$node->tree = $this->_tree_id;
		return $node;
	}
	
	public function findNode($by)
	{
		$nodes_by_id = $this->_storage->getNode(array('id' => (int)$by));
		
		$nodes_by_slug = $this->_storage->getNode(array('slug' => $by));
		
		$nodes_by_name = $this->_storage->getNode(array('name' => $by));
		
		return array_merge($nodes_by_id, $nodes_by_slug, $nodes_by_name);
	}
	
	public function addNode(Node $to, Node $newNode)
	{
		$nodes = $this->_storage->getNode(array('id' => (int)$to->id));
		
		if ( count($nodes) == 0 )
		{
			$this->_errors['addNode'] = 'inexistent_parent';
			return false;
		}
		
		$to = $nodes[0];
		
		$newNode->tree = $to->tree;
		$newNode->parent = $to->id;
		$newNode->path = $to->path;
		$newNode->path[] = $to->id;
		
		try
		{
			$this->_storage->begin_transaction();
			$newNode = $this->_storage->addNode($newNode);
			
			$to->children[] = $newNode->id;
			$this->_storage->updateNode($to);
			
			$this->_storage->commit_transaction();
		}
		catch(Exception $e)
		{
			$this->_storage->rollback_transaction();
			$this->_errors['addNode'] = $e->getMessage();
			
			return false;
		}
		
		return $newNode;
	}
	
	public function updateNode(Node $node)
	{
		return $this->_storage->updateNode($node);
	}
	
	public function deleteNode(Node $node, $delete_subtree = false)
	{	
		// Get the flat subtree
		$subtree = $this->_storage->getTree($node);
		
		// Get parent
		$parent = $this->getNode($node->parent);
		
		$parent->children = array_diff($parent->children, array($node->id));
		$this->_storage->begin_transaction();
		
		try
		{
			$this->_storage->updateNode($parent);
			
			if ( (boolean)$delete_subtree )
			{
				$ids = array();
				foreach( $subtree as $subnode )
				{
					$ids[] = $subnode->id;
				}
				$ids[] = $node->id;
				
				$this->_storage->massDelete($ids);
			}
			else
			{
				foreach( $subtree as $subnode )
				{
					if ( $subnode->parent == $node->id )
					{
						$subnode->parent = $this->_root->id;
						$this->_root->children[] = $subnode->id;
					}
					
					$subnode->path = array_merge(
						array($this->_root->id),
						array_diff($subnode->path, array_merge($node->path, array($node->id)))
					);
					
					$this->_storage->updateNode($subnode);
				}
				
				$this->_storage->updateNode($this->_root);
				
				$this->_storage->deleteNode($node);
			}
			
			$this->_storage->commit_transaction();
			
			return true;
		}
		catch(Exception $e)
		{
			$this->_storage->rollback_transaction();
			$this->_errors['deleteNode'] = $e->getMessage();
			return false;
		}
	}
	
	public function move(Node $node, Node $to, $order = 1)
	{
		$subtree = $this->getTree($node, true);
		
		if ( $order < 1 ) $order = 1;
		if ( $order > count($to->children) ) $order = count($to->children);
		
		if ( array_key_exists($to->id, $subtree) )
		{
			$this->_errors['move'] = 'cannot_move_to_child';
			return false;
		}
		
		if ( $node->parent == $to->id )
		{
			if ( $to->children[$order-1] == $node->id ) return true;
			
			$children = array_diff($to->children, array($node->id));
			$arr2 = array_slice($children, $order-1);
			$arr1 = array_diff($children, $arr2);
			$to->children = array_merge($arr1, array($node->id), $arr2);
			
			$this->_storage->updateNode($to);
			
			return true;
		}
		else
		{
			// Get old parent
			$old_parent = $this->getNode($node->parent);
			
			$old_parent->children = array_diff($old_parent->children, array($node->id));
			$this->_storage->begin_transaction();
			
			try
			{
				$this->_storage->updateNode($old_parent);
				
				$arr2 = array_slice($to->children, $order-1);
				$arr1 = array_diff($to->children, $arr2);
				$to->children = array_merge($arr1, array($node->id), $arr2);
				
				$this->_storage->updateNode($to);
				
				$node->parent = $to->id;
				$node->path = array_merge($to->path, array($to->id));
				$this->_storage->updateNode($node);
				
				foreach( $subtree as $subnode )
				{
					$subnode->path = array_merge(
						$to->path,
						array($to->id, $node->id),
						array_diff($subnode->path, array_merge($node->path, array($node->id)))
					);
					
					$this->_storage->updateNode($subnode);
				}
				
				$this->_storage->commit_transaction();
				
				return true;
			}
			catch(Exception $e)
			{
				$this->_storage->rollback_transaction();
				$this->_errors['move'] = $e->getMessage();
				return false;
			}
		}
	}
	
	public function getTree(Node $root, $flat = false)
	{
		$data = $this->_storage->getTree($root);
		
		$tmp = array();
		foreach( $data as $node )
		{
			$tmp[$node->id] = $node;
		}
		$data = $tmp;
		
		if ( $flat ) return $data;
		
		$tree = $this->_build_tree($root, $data);
		
		return $tree;
	}
	
	public function getErrors()
	{
		return $this->_errors;
	}
	
	public function getRoot()
	{
		return $this->_root;
	}
	
	private function _build_tree(Node $root, $data)
	{
		$mapped_node = array(
			'node' => $root,
			'subnodes' => array()
		);
		
		if ( !empty($root->children) )
		{
			foreach( $root->children as $child )
			{
				if ( array_key_exists($child, $data) )
				{
					$mapped_node['subnodes'][] = $this->_build_tree($data[$child], $data);
				}
			}
		}
		
		return $mapped_node;
	}
}
