<?php

namespace InfinityTree;

/**
 * Simple class that uses basic filtering to clean the input used in InfinityTree
 */
class InputFilter
{
	/**
	 * Cleans the input
	 * Can recursevly clean an array
	 * @param mixed $input Can be an array or a simple input
	 * @return mixed Returns the cleaned input
	 */
	public static function clean($input)
	{
		if ( is_bool($input) ) return $input;
		
		if ( is_array($input) )
		{
			foreach( $input as $key => $value )
			{
				$input[$key] = self::clean($value);
			}
		}
		else
		{
			$input = trim($input);
			$input = strip_tags($input);
			$input = self::sql_escape($input);
		}
		
		return $input;
	}	
	
	private static function sql_escape($value) {
		$return = '';
		for($i = 0; $i < strlen($value); ++$i) 
		{
			$char = $value[$i];
			$ord = ord($char);
			
			if($char !== "'" && $char !== "\"" && $char !== '\\' && $ord >= 32 && $ord <= 126)
				$return .= $char;
			else
				$return .= '\\x' . dechex($ord);
		}
		return $return;
	}
}
