<?php

namespace InfinityTree\Interfaces;

use InfinityTree\Node;

/**
 * Interface handling the validation of tree related actions
 */
interface Validation
{
	/**
	 * Checks if a node is valid
	 * @param Node $node The incoming node
	 * @return boolean
	 */
	public function validNode(Node $node);
	
	/**
	 * @return array Validation errors
	 */
	public function getErors();
}