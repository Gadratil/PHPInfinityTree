<?php

namespace InfinityTree\Interfaces;

interface Conversion
{
	/**
	 * Converts a tree array into html or other formats
	 * @param array $tree The tree to convert
	 * @param mixed $to The output format
	 * @return mixed Returns the output in the given format
	 */
	public function convert(Array $tree, $to);
}