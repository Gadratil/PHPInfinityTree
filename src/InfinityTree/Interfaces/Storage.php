<?php

namespace InfinityTree\Interfaces;

use InfinityTree\Node;

interface Storage
{
	/**
	 * Checks if the storage exists and can be used
	 * @return boolean
	 */
	public function exists();
	
	/**
	 * Generates the storage requirements
	 * @return boolean
	 */
	public function create_storage();
	
	/**
	 * Destroys the created storage if needed
	 * @return boolean
	 */
	public function destroy_storage();
	
	/**
	 * Gets node / nodes by given params
	 * @param array $by Associative array with params to use
	 * @return array Always returns an array
	 */
	public function getNode(Array $by);
	
	/**
	 * Adds a node to storage
	 * @param InfinityTree\Node $node The node to add
	 * @return InfinityTree\Node Fills id and other things to the node and returns it
	 */
	public function addNode(Node $node);
	
	/**
	 * Updates one node
	 * @param InfinityTree\Node $node Node to update
	 * @return boolean
	 */
	public function updateNode(Node $node);
	
	/**
	 * Deletes one node
	 * @param InfinityTree\Node $node The node to delete
	 * @return boolean
	 */
	public function deleteNode(Node $node);
	
	/**
	 * Gets a tree / subtree with the given root, not mapped
	 * @param InfinityTree\Node $node The root node
	 * @return array The array with nodes
	 */
	public function getTree(Node $root);
	
	/**
	 * Updates more nodes at one time
	 * @param array $by The associative names of properties to update by
	 * @param array $data The new values in associative form
	 * @return boolean
	 */
	public function massUpdate(Array $by, Array $data);
	
	/**
	 * Deletes nodes based on ids
	 * @param array $ids
	 * @return boolean
	 */
	public function massDelete(Array $ids);
	
	/**
	 * Checks if a given slug exists
	 * @param InfinityTree\Node $node
	 * @return boolean
	 */
	public function slugExists(Node $node);
	
	/**
	 * Begins transaction
	 */
	public function begin_transaction();
	
	/**
	 * Commits the changes made
	 */
	public function commit_transaction();
	
	/**
	 * Rolls back the changes if something went wrong
	 */
	public function rollback_transaction();
}