<?php

namespace InfinityTree;

use InfinityTree\Interfaces\Storage as StorageInterface;
use InfinityTree\Node;

/**
 * Soul purpose of this class is to create a unique slug for each node in a tree
 */
class Slugifier
{
	private $_storage = null;
	
	public function __construct(StorageInterface $storage)
	{
		$this->_storage = $storage;
	}
	
	public function generateSlug(Node $node)
	{
		$node->slug = $this->_convert_to_slug($node->name);
		
		if ( $this->_storage->slugExists($node) )
		{
			$base = $node->slug;
			$counter = 1;
			$node->slug = $base.'-'.$counter;
			while( $this->_storage->slugExists($node) )
			{
				$counter++;
				$node->slug = $base.'-'.$counter;
			}
		}
		
		return $node;
	}
	
	/**
	  * Slugify a given string
	  * Example: John's cat -> johns-cat
	  *
	  * @param string The text you want to convert
	  * @return string 
	  */
	private static function _convert_to_slug($str)	
	{
		$str = strtolower(trim($str));
		$str = preg_replace('/[^a-z0-9-]/', '-', $str);
		$str = preg_replace('/-+/', "-", $str);

		return $str;
	}
}	