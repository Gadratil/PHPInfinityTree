<?php

namespace InfinityTree\Adapters;

use ValidationWall\Door\PredefDoor;
use ValidationWall\RuleSet\PredefRuleset;
use ValidationWall\Rule\NotEmpty;
use InfinityTree\Node;
use ValidationWall;

class Validation
{
	private $_errors = array();
	
	public function validNode(Node $node)
	{
		$rulesets = array();
		$rulesets[] = new PredefRuleset('name', array(new NotEmpty()));
		
		$door = new PredefDoor($rulesets);
		
		$vw = new ValidationWall($door);
		
		if ( $vw->pass((array)$node) )
		{
			return true;
		}
		else
		{
			$this->_errors = $door->getErrors();
			return false;
		}
	}

	public function getErrors()
	{
		return $this->_errors;
	}
}
