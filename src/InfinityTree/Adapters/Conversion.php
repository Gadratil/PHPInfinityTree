<?php

namespace InfinityTree\Adapters;

use InfinityTree\Interfaces\Conversion as ConversionInterface;

class Conversion implements ConversionInterface
{
	const TO_UL = 1;
	const TO_SELECT = 2;
	
	public function convert(Array $tree, $to = self::TO_UL)
	{
		switch($to)
		{
			case self::TO_UL:
				return $this->_convert_to_ul($tree);
			case self::TO_SELECT:
				return $this->_convert_to_select($tree);
			default:
				return $tree;
		}
	}
	
	private function _convert_to_ul($tree)
	{
		$ul = '<ul class="infinityTree">';
		$ul .= '<li id="IT_node_'.$tree['node']->id.'" class="IT-node">'.$tree['node']->name;
		if ( count($tree['subnodes']) > 0 )
		{
			$ul .= $this->_recursive_traverse(
				$tree['subnodes'],
				'<ul class="IT-subtree">',
				'</ul>',
				'<li class="IT-node" id="IT_node_::node_id::">',
				'</li>'
			);
		}
		$ul .= '</li>';
		$ul .= '</ul>';
		return $ul;
	}
	
	private function _convert_to_select($tree)
	{
		$select = '<select class="infinityTree">';
		$select .= '<option id="IT_node_'.$tree['node']->id.'">'.$tree['node']->name.'</option>';
		if ( count($tree['subnodes']) > 0 )
		{
			$select .= $this->_recursive_traverse(
				$tree['subnodes'],
				'',
				'',
				'<option class="IT-node" id="IT_node_::node_id::">::space::-',
				'</option>'
			);
		}
		$select .= '</select>';
		return $select;
	}
	
	private function _recursive_traverse($children, $a_b, $a_a, $i_b, $i_a, $depth = 1)
	{
		$str = $a_b;
		foreach ( $children as $node )
		{
			$i_b_c = str_replace('::node_id::', $node['node']->id, $i_b);
			$i_b_c = str_replace('::space::',str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $depth) , $i_b_c);
			$str .= $i_b_c.$node['node']->name;
			if ( count($node['subnodes']) > 0 )
			{
				$str .= $this->_recursive_traverse(
					$node['subnodes'],
					$a_b,
					$a_a,
					$i_b,
					$i_a,
					$depth + 1
				);
			}
			$str .= $i_a;
		}
		$str .= $a_a;
		
		return $str;
	}
}