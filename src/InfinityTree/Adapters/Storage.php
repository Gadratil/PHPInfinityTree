<?php

namespace InfinityTree\Adapters;

use InfinityTree\Interfaces\Storage as StorageInterface;
use DBHandler;
use InfinityTree\Node;

class Storage implements StorageInterface
{
	private $_db = null;
	
	private $_table_name = 'infinity_tree';
	
	public function __construct(Array $config)
	{
		$this->_db = new DBHandler($config);
		
		if ( array_key_exists('table_name', $config) )
		{
			$this->_table_name = $config['table_name'];
		}
	}
	
	public function exists()
	{
		$res = $this->_db->custom_query('SHOW TABLES LIKE "'.$this->_table_name.'"');
		if ( is_array($res) && count($res) > 0 ) return true;
		return false;
	}
	
	public function create_storage()
	{
		$sql = "CREATE TABLE `".$this->_table_name."` (
			`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
			`parent` INT UNSIGNED NOT NULL DEFAULT '0',
			`tree` INT UNSIGNED NOT NULL DEFAULT '0',
			`slug` VARCHAR(255) NOT NULL,
			`name` VARCHAR(255) NOT NULL,
			`path` TEXT NOT NULL,
			`children` TEXT NOT NULL,
			PRIMARY KEY (`id`)
		)
		COLLATE='utf8_unicode_ci'
		ENGINE=InnoDB;
		";
		
		if ( $this->_db->custom_query($sql) )
		{
			return true;
		}
		
		return false;
	}
	
	public function destroy_storage()
	{
		$this->_db->query('DROP TABLE IF EXISTS '.$this->_table_name);
	}
	
	public function getNode(Array $by)
	{
		$extra_sql = '';
		foreach( $by as $key => $value )
		{
			$extra_sql .= ' AND `'.$key.'` = '.((is_numeric($value))? $value : '"'.$value.'"');
		}
		
		$result = $this->_db->get_table_data($this->_table_name, null, null, $extra_sql);
		
		$tmp = array();
		foreach( $result as $node_arr )
		{
			$node = new Node();
			$node->inject_data($node_arr);
			$node->path = array_filter(explode('-', str_replace('|', '', $node->path)));
			$node->children = array_filter(explode(',', str_replace('|', '', $node->children)));
			$tmp[] = $node;
		}
		
		return $tmp;
	}
	
	public function addNode(Node $node)
	{
		$node->path = '|'.implode('-', (array)$node->path).'|';
		$node->children = '|'.implode(',', (array)$node->children).'|';
		$node_arr = (array)$node;
		unset($node_arr['id']);
		
		$node->id = $this->_db->insert_data($this->_table_name, $node_arr);
		
		$node->path = array_filter(explode('-', str_replace('|', '', $node->path)));
		$node->children = array_filter(explode(',', str_replace('|', '', $node->children)));
		
		return $node;
	}
	
	public function updateNode(Node $node)
	{
		$node->path = '|'.implode('-', (array)$node->path).'|';
		$node->children = '|'.implode(',', (array)$node->children).'|';
		$node_arr = (array)$node;
		$fields = $node_arr;
		unset($fields['id']);
		$fields = array_keys($fields);
		
		$node->path = array_filter(explode('-', str_replace('|', '', $node->path)));
		$node->children = array_filter(explode(',', str_replace('|', '', $node->children)));
		
		return $this->_db->update_data(
			$this->_table_name,
			$node_arr,
			$fields,
			array('id')
		);
	}
	
	public function deleteNode(Node $node)
	{
		return $this->_db->delete_data(
			$this->_table_name,
			array('id' => $node->id),
			array('id')
		);
	}
	
	public function getTree(Node $root)
	{
		$sql = 'SELECT * FROM `'.$this->_table_name.'` WHERE 
			`path` LIKE "%-:parent-%"
			OR `path` LIKE "%|:parent-%"
			OR `path` LIKE "%-:parent|%"
			OR `path` LIKE "%|:parent|%"';
			
		$result = $this->_db->custom_query($sql, array('parent' => $root->id));
		
		$tmp = array();
		foreach( $result as $node_arr )
		{
			$node = new Node();
			$node->inject_data($node_arr);
			$node->path = array_filter(explode('-', str_replace('|', '', $node->path)));
			$node->children = array_filter(explode(',', str_replace('|', '', $node->children)));
			$tmp[] = $node;
		}
		
		return $tmp;
	}
	
	public function massUpdate(Array $by, Array $data)
	{
		return $this->_db->update_data(
			$this->_table_name,
			array_merge($by, $data),
			array_keys($data),
			array_keys($by)
		);
	}
	
	public function massDelete(Array $ids)
	{
		return $this->_db->delete_data(
			$this->_table_name,
			array('id' => $ids),
			array('id')
		);
	}
	
	public function slugExists(Node $node)
	{
		$sql = 'SELECT COUNT(`id`)AS nr FROM '.$this->_table_name.' 
			WHERE `slug` = :slug
			AND `tree` = :tree_id
			AND `id` != :id';
			
		$result = $this->_db->custom_query($sql, array('slug' => $node->slug, 'tree_id' => $node->tree, 'id' => (int)$node->id));
		
		if ( $result AND is_array($result) AND count($result) > 0 )
		{
			return $result[0]['nr'] > 0;
		}
		
		return false;
	}
	
	public function begin_transaction()
	{
		$this->_db->begin_transaction();
	}
	
	public function commit_transaction()
	{
		$this->_db->commit_transaction();
	}
	
	public function rollback_transaction()
	{
		$this->_db->rollback_transaction();
	}
}
