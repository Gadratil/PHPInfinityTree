<?php
// Config file that returns a simple array with the desired configuration parameters

return array(
	'db' => array( // DB Config for DB storage
		'server' => 'localhost', // Host you want to connect to, like localhost
		'user' => 'root', // DB user
		'pass' => 'root', // DB password
		'database' => 'test', // Name of the database
	),
);
