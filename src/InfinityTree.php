<?php

use InfinityTree\Interfaces\Storage as StorageInterface;
use InfinityTree\Interfaces\Validation as ValidationInterface;
use InfinityTree\Interfaces\Conversion as ConversionInterface;
use InfinityTree\Adapters\Storage as StorageAdapter;
use InfinityTree\Adapters\Validation as ValidationAdapter;
use InfinityTree\Adapters\Conversion as ConversionAdapter;
use InfinityTree\Tree;
use InfinityTree\Node;
use InfinityTree\Slugifier;
use InfinityTree\InputFilter;

/**
 * A library for handling tree structure with infinite depth
 * @author �rd�g Atilla <attila.ordog@yahoo.com>
 */
class InfinityTree
{
	/**
	 * @var array $_config Holds the configuration array for this library
	 */
	private $_config = array(
		'db' => array(
			'server' => '',
			'user' => '',
			'pass' => '',
			'database' => '',
			'table_name' => ''
		)
	);
	
	/**
	 * @var InfnityTree\Interfaces\Storage $_storage Holds the implemented storage adapter
	 */
	private $_storage = null;
	
	/**
	 * @var InfinityTree\Interfaces\Validation $_validation Holds the implemented validation adapter
	 */
	private $_validation = null;
	
	/**
	 * @var InfinityTree\Tree $_tree Holds the tree we work on
	 */
	private $_tree = null;
	
	/**
	 * @var array An associative array with the errors happening during tree handling
	 */
	private $_errors = array();
	
	/**
	 * Constructor
	 * If you want, you can create your own adapters based on the interfaces
	 * @param array $config Config array, optinal, rewrites rules loaded from file
	 * @param InfinityTree\Interfaces\Storage $storage The adapter for the storage
	 * @param InfinityTree\Interfaces\Validation $validation The validation adapter
	 */
	public function __construct(
		Array $config = array(), 
		StorageInterface $storage = null,
		ValidationInterface $validation = null
	)
	{
		$file = dirname(__FILE__).'/config.php';
		
		$file_config = array();
		if ( file_exists($file) )
		{
			$file_config = include($file);
		}
		
		foreach ( $this->_config as $key => $value )
		{
			if ( array_key_exists($key, $file_config) )
			{
				$this->_config[$key] = $file_config[$key];
			}
			
			if ( array_key_exists($key, $config) )
			{
				$this->_config[$key] = $config[$key];
			}
		}
		
		if ( $storage === null )
		{
			$this->_storage = new StorageAdapter($this->_config['db']);
			if ( !$this->_storage->exists() ) 
			{
				$this->_storage->create_storage();
			}
		}
		else
		{
			$this->_storage = $storage;
		}
		
		if ( $validation === null )
		{
			$this->_validation = new ValidationAdapter();
		}
		else
		{
			$this->_validation = $validation;
		}
		
		$this->_tree = new Tree(1, $this->_storage);
	}
	
	/**
	 * Initializes a tree
	 * It is important to do so, otherwise the default tree is used with the id of 1
	 * This makes possible to have more trees, like categories and chapters with one library
	 * @param int $id The id of the tree you want to use
	 */
	public function initTree($id)
	{
		$id = InputFilter::clean($id);
		
		$this->_tree = new Tree((int)$id, $this->_storage);
	}
	
	/**
	 * Find a node by ID, slug or name
	 * @param mixed $by Can be either the id, the slug or the name of the node
	 * @return array Returns all the found nodes or empty array if none found
	 */
	public function findNode($by)
	{
		$by = InputFilter::clean($by);
		
		return $this->_tree->findNode($by);
	}
	
	/**
	 * Creates a node and adds it to the initialized tree
	 * @param int $parent The parent node's ID to add to
	 * @param string $name Name of the node
	 * @return InfinityTree\Node Returns the created node
	 */
	public function addNode($parent = null, $name)
	{
		$parent = InputFilter::clean($parent);
		$name = InputFilter::clean($name);
		
		$node = new Node();
		
		if ( $parent == null )
		{
			$parent = $this->_tree->getRoot();
		}
		else
		{
			$parent = $this->_tree->getNode((int)$parent);
			if ( $parent->id == null )
			{
				$this->_errors['parent'] = 'Parent inexistent';
				return $node;
			}
		}
		
		$node->name = $name;
		$node->tree = $parent->tree;
		
		if ($this->_validation->validNode($node))
		{
			// Create slug and add
			$slugifier = new Slugifier($this->_storage);
			$node = $slugifier->generateSlug($node);
			
			return $this->_tree->addNode($parent, $node);
		}
		else
		{
			$this->_errors = $this->_validation->getErrors();
			return $node;
		}
	}
	
	/**
	 * Updates the name of the node by id
	 * @param int $id The id of the node to update (cannot be root)
	 * @param string $name The new name of the node
	 * @return boolean
	 */
	public function updateNode($id, $name)
	{
		$id = InputFilter::clean($id);
		$name = InputFilter::clean($name);
		
		$node = $this->_tree->getNode((int)$id);
		if ( $node->id == null )
		{
			$this->_errors['node'] = 'Node inexistent';
			return false;
		}
		
		if ( $node->slug == 'root' )
		{
			$this->_errors['node'] = 'Cannot update root';
			return false;
		}
		
		$node->name = (string)$name;
		
		$slugifier = new Slugifier($this->_storage);
		$node = $slugifier->generateSlug($node);
		
		return $this->_tree->updateNode($node);
	}
	
	/**
	 * Deletes a node
	 * @param int $id The id of the node
	 * @param boolean $delete_subtree If true, deletes the whole subtree, otherwise moves subtree to root
	 * @return boolean
	 */
	public function deleteNode($id, $delete_subtree = false)
	{
		$id = InputFilter::clean($id);
		$delete_subtree = InputFilter::clean($delete_subtree);
		
		$node = $this->_tree->getNode((int)$id);
		if ( $node->id == null )
		{
			$this->_errors['node'] = 'Node inexistent';
			return false;
		}
		
		if ( $node->slug == 'root' )
		{
			$this->_errors['node'] = 'Cannot delete root';
			return false;
		}
		
		return $this->_tree->deleteNode($node, $delete_subtree);
	}
	
	/**
	 * Moves a node under another node
	 * Can be moved under the same parent with the goal of changing order
	 * @param int $node Id of the node to move
	 * @param int $to Id of the node to move to, root if null
	 * @param int $order The place of the moved node on the level it is on
	 * Use order like: 2, this means the node will be moved to the second place
	 * @return boolean
	 */
	public function moveNode($node, $to, $order = 1)
	{
		$node = InputFilter::clean($node);
		$to = InputFilter::clean($to);
		$order = InputFilter::clean($order);
		
		$node = $this->_tree->getNode((int)$node);
		if ( $node->id == null )
		{
			$this->_errors['node'] = 'Node inexistent';
			return false;
		}
		
		if ( $node->slug == 'root' )
		{
			$this->_errors['node'] = 'Cannot move root';
			return false;
		}
		
		if ( $to == null ) 
		{
			$to = $this->_tree->getRoot();
		}
		else
		{
			$to = $this->_tree->getNode((int)$to);
			if ( $to->id == null )
			{
				$this->_errors['node'] = 'Node inexistent';
				return false;
			}
		}
		
		return $this->_tree->move($node, $to, (int)$order);
	}
	
	/**
	 * Returns the tree beginning from given root id
	 * @param int $root The node to start the tree from
	 * @param boolean $flat If true, it returns all the children of a given root in a flat array
	 * @return array
	 */
	public function treeAsArray($root = null, $flat = false)
	{
		$root = InputFilter::clean($root);
		$flat = InputFilter::clean($flat);
		
		if ( $root == null )
		{
			$root = $this->_tree->getRoot();
		}
		else
		{
			$root = $this->_tree->getNode((int)$root);
			if ( $root->id == null )
			{
				$this->_errors['root'] = 'Node inexistent';
				return array();
			}
		}
		
		return $this->_tree->getTree($root, $flat);
	}
	
	/**
	 * Converts a tree into html or other formats
	 * @param array $tree The tree in array format
	 * @param string $to The param that decides the output
	 * @param ConversionInterface $conversion If you want, you can pass your own conversion class, it just has to implement the interface
	 * @return mixed Returns the output in the format required
	 */
	public function convertTo(Array $tree, $to, ConversionInterface $conversion = null)
	{
		$to = InputFilter::clean($to);
		
		if ( $conversion == null ) $conversion = new ConversionAdapter();
		
		return $conversion->convert($tree, $to);
	}
	
	public function getErrors()
	{
		return $this->_errors;
	}
}
