<?php
/**
 * Autoload file that needs to be laoded to use InfinityTree
 * In this autoload type the file name has to match the class name
 * Since we use namespaces, we always have to add "use" if we want to use a class
 */

if (!defined('INFINITYTREE_ROOT')) {
    define('INFINITYTREE_ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
}

spl_autoload_register('autoload');

function autoload($class)
{	
	if ( class_exists($class,FALSE) ) {
		// Already loaded
		return FALSE;
	}
	
	$class = str_replace('\\', '/', $class);

	if ( file_exists(INFINITYTREE_ROOT.$class.'.php') )
	{
		require(INFINITYTREE_ROOT.$class.'.php');
	}

	return false;
}

//Also require vendors for this project
require_once(INFINITYTREE_ROOT.'Vendor/DBHandler/DBHandler.php');
require_once(INFINITYTREE_ROOT.'Vendor/ValidationWall/autoload.php');