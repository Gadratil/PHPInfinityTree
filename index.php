<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PHP InfinityTree</title>

    <!-- Bootstrap -->
    <link href="assets/vendor/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/jqTree/jqtree.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<nav class="navbar navbar-default">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">PHPIT</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="active"><a href="/">Example</a></li>
					<li><a href="/documentation.html">Doc for users</a></li>
					<li><a href="/documentation/index.html">Doc for devs</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<h1>PHP InfinityTree</h1>
		
		<div class="row">
			<div class="col-xs-12">
				<h2>Introduction</h2>
				<p>PHP Infinity Tree is an infinite depth tree plugin. It can contain multiple trees of any depth. Data storage is easy, PHP handles the recursive depth rendering.</p>
				<p>This example uses jqTree jQuery plugin to render the tree. You can use the code of the example, just get the request.php and InfinityTree.js files to see how it works.</p>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 tree-container">
				<div class="tree" id="tree">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>Moving nodes</h3>
				<p>
					The front uses jqTree to display the tree. Move the nodes to rearrange the tree.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>New node</h3>
				<p>
					Select a node, enter the name of the new node and push submit. You will see the created node under the selected node.
				</p>
				<form class="col-xs-12 col-md-6 IT-form" data-action="createNode">
					<div class="form-group">
						<input type="text" class="form-control" id="new_node" placeholder="New Node">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>Delete node</h3>
				<p>
					Select a node and press delete, you will see the updated tree if successful. If you delete a node that has a subtree, subtree gets moved to root. For more options, see Documentation.
				</p>
				<form class="col-xs-12 col-md-6 IT-form" data-action="deleteNode">
					<div class="form-group">
					</div>
					<button type="submit" class="btn btn-default">Delete</button>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3>Update node</h3>
				<p>
					Select a node and enter the new name you want to use for it.
				</p>
				<form class="col-xs-12 col-md-6 IT-form" data-action="updateNode">
					<div class="form-group">
						<input type="text" class="form-control" id="update_node" placeholder="Update Name">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/vendor/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/vendor/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <script src="assets/vendor/jqTree/tree.jquery.js"></script>
    <script src="assets/js/infinityTree.js"></script>
  </body>
</html>
