<?php
/**
 * File that handles requests for the project's sake
 */
include('vendor/autoload.php');

$response = array(
	'success' => false,
	'data' => array(),
	'errors' => array() 
);

$request = array(
	'action' => '',
	'params' => array()
);

$tree = new InfinityTree();
$tree->initTree(1);

$convertToUl = InfinityTree\Adapters\Conversion::TO_UL;

if ($_POST)
{
	foreach ( $request as $key => $value )
	{
		if ( array_key_exists($key, $_POST) )
		{
			$request[$key] = $_POST[$key];
		}
	}
	
	switch($request['action'])
	{
		case 'getTree':
			$response['success'] = true;
			$response['data'] = $tree->treeAsArray();
			break;
		case 'createNode':
		{ // Handling the node creation
			$req_fields = array('parent', 'name');
			foreach ( $req_fields as $field )
			{
				if ( !array_key_exists($field, $request['params']) )
				{
					$request['params'][$field] = null;
				}
			}
			
			$newNode = $tree->addNode($request['params']['parent'], $request['params']['name']);
			if ( $newNode->id == null )
			{
				$response['errors'] = $tree->getErrors();
			}
			else
			{
				$response['success'] = true;
			}
			break;
		}
		case 'deleteNode':
		{
			// Deleting a node
			$req_fields = array('id');
			foreach ( $req_fields as $field )
			{
				if ( !array_key_exists($field, $request['params']) )
				{
					$request['params'][$field] = null;
				}
			}
			
			$response['success'] = $tree->deleteNode((int)$request['params']['id']);
			$response['errors'] = $tree->getErrors();
			break;
		}
		case 'updateNode':
		{
			//Updating a node
			$req_fields = array('id', 'name');
			foreach ( $req_fields as $field )
			{
				if ( !array_key_exists($field, $request['params']) )
				{
					$request['params'][$field] = null;
				}
			}
			$response['success'] = $tree->updateNode(
				(int)$request['params']['id'],
				(string)$request['params']['name']
			);
			$response['errors'] = $tree->getErrors();
			break;
		}
		case 'moveNode':
		{
			// Moving node
			$req_fields = array('id', 'to', 'position');
			foreach ( $req_fields as $field )
			{
				if ( !array_key_exists($field, $request['params']) )
				{
					$request['params'][$field] = null;
				}
			}
			$response['success'] = $tree->moveNode(
				(int)$request['params']['id'],
				(int)$request['params']['to'],
				(int)$request['params']['position']
			);
			$response['errors'] = $tree->getErrors();
			break;
		}
		default:
			$response['errors'] = 'Invalid action';
			break;
	}
	
	echo json_encode($response);
	exit;
}
else
{
	echo '';
	exit;
}
?>
