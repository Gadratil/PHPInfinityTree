var infinityTree = {
	init : function()
	{
		this.initTree();
		this.initEvents();
	},
	
	initTree : function()
	{
		$('#tree').tree('destroy');
		$('#tree').remove();
		$('.tree-container').html('');
		$('.tree-container').append('<div id="tree"></div>');
		this.request.action = 'getTree';
		this.performRequest();
		if ( this.response.success )
		{
			this.currentTree.push(this.recursiveBuildTree(this.response.data));	
		}
		
		var data = this.currentTree;
		$('#tree').tree({
			data:data,
			dragAndDrop: true,
  			autoOpen: true,
  			onCreateLi: function(node, $li) {
			    $li.attr('id', 'node_'+node.id);
			}
		});
		var context = this;
		$('#tree').bind(
		    'tree.move',
		    function(event) {
		    	event.preventDefault();
		    	context.request.action='moveNode';
		    	context.request.params = {
		    		'id' : event.move_info.moved_node.id,
		    		'to' : 0,
		    		'position' : 1
		    	};
			
			if ( event.move_info.position == 'inside' ) 
			{
				context.request.params.to = event.move_info.target_node.id;
			}
			else
			{
				context.request.params.to = event.move_info.target_node.parent.id;
			}
			
			event.move_info.do_move();
			
			var index = $('#node_'+event.move_info.moved_node.id).index();
			console.log(index);
			context.request.params.position = index + 1;
			
			context.performRequest();
			location.reload();
		    }
		);
	},
	
	request : {
		'action' : '',
		'params' : {}
	},
	
	response : {
		'success' : false,
		'data' : {},
		'errors' : {}
	},

	currentTree : [],
	
	performRequest : function()
	{
		var context = this;
		
		$.ajax({
			crossDomain : true,
			url : '../../request.php',
			type : 'POST',
			data : context.request,
			cache : false,
			async : false,
			dataType : 'JSON',
			statusCode : {
				404 : function(){
					alert('Request handler not available');
				},
				0 : function(){
					alert('Request handler not available');
				}
			}
		}).done(function(data){
			context.response = data;
		}).fail(function(jqXHR, textStatus, error){
			context.response.success = false;
			context.response.errors = jqXHR.statusText;
			alert(context.response.errors);
		});
	},
	
	initEvents : function()
	{
		var context = this;
		
		$('body').on('submit', '.IT-form', function(e){
			e.preventDefault();
			
			var currentForm = $(this);
			var action = $(this).data('action');
			
			context.request.action = action;
			
			var selected = null;
			var node = $('#tree').tree('getSelectedNode');
			
			if ( node.id.length )
			{
				selected = node.id;
				
			}
			
			switch(action)
			{
				case 'createNode':
				{	
					context.request.params = {
						'parent' : selected, 
						'name' : $('#new_node').val()
					};
					context.performRequest();
					if ( context.response.success )
					{
						$('input[type="text"]').val('');
						location.reload();
					}
					else
					{
						if ( context.response.errors )
						{
							context.displayErrors(currentForm);
						}
					}
					break;
				}
				case 'deleteNode':
				{
					context.request.params = {
						'id' : selected
					};
					context.performRequest();
					if ( context.response.success )
					{
						$('input[type="text"]').val('');
						location.reload();
					}
					else
					{
						if ( context.response.errors )
						{
							context.displayErrors(currentForm);
						}
					}
					break;
				}
				case 'updateNode':
				{
					context.request.params = {
						'id' : selected, 
						'name' : $('#update_node').val()
					};
					context.performRequest();
					if ( context.response.success )
					{	
						$('input[type="text"]').val('');
						location.reload();
					}
					else
					{
						if ( context.response.errors )
						{
							context.displayErrors(currentForm);
						}
					}
					break;
				}
			}
			
			return false;
		});
	},
	
	recursiveBuildTree : function(data) {
		var node = {id : data.node.id, label: data.node.name, children : []};
		for (var i = 0, m = data.subnodes.length; i < m; i++) {
			node.children.push(this.recursiveBuildTree(data.subnodes[i]));
		}
		
		return node;	
	},
	
	displayErrors: function(currentForm)
	{
		var context = this;
		
		currentForm.find('.help-block').remove();
		$.each(context.response.errors, function(index, value){
			currentForm.find('.form-group').append('<span class="help-block" style="color:red;">'+value+'</span>');
		});
	}
}

$(document).ready(function(){
	infinityTree.init();
});
