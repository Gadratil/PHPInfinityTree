# PHPInfinityTree

This simple library is for handling infinite depth trees. The created tree can than be used for categories or displaying folder structures or other data that needs infinite depth.

#### Feature list:

 * Easy to use
 * Data is linear, php creates the depth
 * Quick

#### Requirements

 * MySQLi
 * PHP >5.4

You can find the documentation [here](http://infinitytree.gadratilprogramming.net/documentation.html);
